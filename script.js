'use scrict'

// objet profil
let profil = {
    prenom: 'Philippe',
    nom: 'Fleury',
    sexe: 'H',
    dateNaissance: new Date(1982, 08, 22),
    situationParticuliere: 'non'
}

// date et heure
let now = new Date()
let date = now.getDate();
let month = now.getMonth() + 1;
let year = now.getFullYear();
let hour = now.getHours();
let min = now.getMinutes();
let sec = now.getSeconds();

// obtenir nombre de jours à analyser
let nbJours = parseInt(prompt("Combien de jours voulez-vous analyser"));

// créer un tableau selon le nombre de jours entré
let tab = [];
for (let i = 0; i < nbJours; i++){
	tab.push(parseInt(prompt("Entrez le nombre de consommations d'alcool pour le jour " + (i + 1) + ".")));
}

// total des valeurs du tableau
function totalConso(tab){
	let totalConso = 0;
	for (let i = 0; i < tab.length; i++) {
  	totalConso += tab[i]
    }
    return totalConso;
}

// total ramené sur 7 jours
let total7jours = ((7 * totalConso(tab)) / nbJours)

// plus grande valeur du tableau
let max = Math.max(...tab)

// ratio de jours sans alcool
let joursSansAlcool = 0;
for (let i = 0; i < tab.length; i++) {
    if (tab[i] === 0) {
    joursSansAlcool++
    }
}
let ratioSansAlcool = ((joursSansAlcool)/nbJours) * 100

// ratio de jours excédants
let joursTrop = 0;
if (profil.sexe === 'H') {
    for (let i = 0; i < tab.length; i++) {
        if (tab[i] > 3) {
        joursTrop++
        }
    }
} else if (profil.sexe === 'F') {
    for (let i = 0; i < tab.length; i++) {
        if (tab[i] > 2) {
        joursTrop++
        }
    }
}
let ratioTrop = ((joursTrop)/nbJours) * 100

// recommandations
let recommandationJour;
let recommandationSemaine;

if (profil.situationParticuliere === 'oui') {
    recommandationJour = 0
    recommandationSemaine = 0    
} else if (profil.situationParticuliere === 'non') {
    if (profil.sexe === 'H') {
        recommandationJour = 3
        recommandationSemaine = 15
    } else if (profil.sexe === 'F') {
        recommandationJour = 2
        recommandationSemaine = 10
    }
}


function messageFinal() {
    if (profil.situationParticuliere === 'oui') {
        if (total7jours == 0) {
            return 'Vous respectez les recommandations. Bravo!'
        } else {
            return 'Vous ne respectez pas les recommandations.'
        }           
    } else if (profil.situationParticuliere === 'non') {
        if (profil.sexe === 'H') {
            if (total7jours <= 15 && max <= 3) {
                return 'Vous respectez les recommandations. Bravo!'
            } else {
                return 'Vous ne respectez pas les recommandations.'
            }           
        } else if (profil.sexe === 'F') {
            if (total7jours <= 10 && max <= 2) {
                return 'Vous respectez les recommandations. Bravo!'
            } else {
                return 'Vous ne respectez pas les recommandations.'
            }       
        }
    }
}



// rapport final
function rapport(){
    console.log('Rapport émis le ' + date + '/' + month + '/' + year + ' à ' + hour + ':' + min + ':' + sec);
    console.log('Nom : ' + profil.nom + ', ' + profil.prenom);
    console.log('Âge : ' + Math.floor((now - profil.dateNaissance)/31557600000) + ' ans');
    console.log('Sexe : ' + profil.sexe);
    console.log('Situation particulière : ' + profil.situationParticuliere);
    console.log('Alcool:');
    console.log('Moyenne par jour : ' + (totalConso(tab)/nbJours).toFixed(2));
    console.log('Consommations sur une semaine : ' + total7jours.toFixed(2) + '   Recommandation : ' + recommandationSemaine);
    console.log('Maximum en une journée : ' + Math.max(...tab) + '  Recommandation : ' + recommandationJour);
    console.log('Ratio journées sans alcool : ' + ratioSansAlcool.toFixed(2) + ' %');
    console.log('Ratio des journées excédants : ' + ratioTrop.toFixed(2) + ' %')
    console.log(messageFinal())
}

console.log(rapport())
